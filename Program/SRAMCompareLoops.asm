SRAMChecks:
  LDA $6000
  EOR #$FF
  STA $6000
  CMP $6000
  BNE .BootDisabled ;SRAM not writeable on boot.
  EOR #$FF
  STA $6000
  LDA #$01 ;Enabled
  STA SRAMBoot
  BNE CheckSRAM
.BootDisabled:
  LDA #$00 ;Disabled
  STA SRAMBoot

CheckSRAM:
  LDA #$00
  JSR BankswitchProgram ;Enable WRAM.
  LDX #$0F
.SRAMAddHeaderLoop: ;Put header inside it.
  LDA HeaderGood,X
  STA $6000,X
  DEX
  BPL .SRAMAddHeaderLoop

  LDX #$0F
.SRAMLoop3:
  LDA $6000,X
  STA SRAMEnabled,X ;Store 16 bytes with this loop to the enabled buffer to check later.
  DEX
  BPL .SRAMLoop3
  
  LDA #$10
  JSR BankswitchProgram ;Disable WRAM
  LDX #$0F
.SRAMLoop4:
  LDA $6000,X
  STA SRAMDisabled,X ;Store 16 bytes with this loop to the enabled buffer to check later.
  DEX
  BPL .SRAMLoop4

  LDX #$0F
.SRAMAddHeaderLoop2: ;Put bad header inside it with WRAM disabled.
  LDA HeaderBad,X
  STA $6000,X
  DEX
  BPL .SRAMAddHeaderLoop2

  LDA #$00
  JSR BankswitchProgram ;Enable WRAM again.
  LDX #$0F
.SRAMLoop5:
  LDA $6000,X
  STA SRAMDisabledWrites,X ;Store 16 bytes with this loop to the enabled buffer to check later.
  DEX
  BPL .SRAMLoop5

  LDX #$0F  
.SRAMInstallCheck:
  LDA SRAMEnabled,X
  CMP HeaderGood,X
  BNE .NoSRAMInstalled ;If enabled has wrong data then enabled writing failed. Change to BEQ to initiate a failure. BNE originally.
  DEX
  BPL .SRAMInstallCheck
  LDA #$02 ;Yes ;Passed test.
  STA SRAMInstalled
  BNE .SRAMCheckDisableable
.NoSRAMInstalled:
  LDA #$03 ;No ;Failed test.
  STA SRAMInstalled
  JMP BankCheck ;SRAM Not writeable, no SRAM at all, so skip other checks.
  
.SRAMCheckDisableable:
  LDX #$0F
.SRAMDisableableCheckLoop:
  LDA SRAMDisabled,X
  CMP HeaderGood,X
  BNE .SRAMDisableablePass ;Data doesn't match so the disable went through and returned open bus and different data.
  DEX
  BPL .SRAMDisableableCheckLoop
  LDA #$03 ;No. Fail test and will not disable. Skip next test as writes will go through too.
  STA SRAMDisableable
  BNE .SRAMCheckDisabledWrites
.SRAMDisableablePass:
  LDA #$02 ;Yes. Passed test and returned different data.
  STA SRAMDisableable
  
.SRAMCheckDisabledWrites:
  LDX #$0F
.SRAMDisabledWritesCheckLoop:
  LDA SRAMDisabledWrites,X
  CMP HeaderGood,X ;Check data against what it should have. BadHeader array was written when disabled.
  BNE .SRAMDisabledWritesTrue ;If data is not equal this is taken because disabled writes was true.
  DEX
  BPL .SRAMDisabledWritesCheckLoop
  LDA #$06 ;Rejected
  STA SRAMDisabledWritesFlag
  BNE BankCheck
.SRAMDisabledWritesTrue:
  LDA #$05 ;Accepted
  STA SRAMDisabledWritesFlag