  CLD          ; disable decimal mode
  LDX #$40
  STX $4017    ; disable APU frame IRQ
  LDX #$FF
  TXS
  INX
  STX PPUCtrl    ; disable NMI
  STX PPUMask    ; disable rendering
  STX $4010    ; disable DMC IRQs
  BIT PPUStatus
.StartupVBlankWait1:
  BIT PPUStatus
  BPL .StartupVBlankWait1
  LDA #$00
  STA $2003
.ClearRAM: ;Also uploads sprites.
  LDA #$00
  STA <$00,X
  STA $0100,X
  STA $0300,X
  STA $0400,X
  STA $0500,X
  STA $0600,X
  STA $0700,X
  LDA #$FF
  STA $0200,X
  STA $2004
  INX
  BNE .ClearRAM
  BIT PPUStatus
.StartupVBlankWait2:      ;Second wait for vblank, PPU is ready after this
  BIT PPUStatus
  BPL .StartupVBlankWait2