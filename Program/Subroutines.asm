GraphicsToPPU:
  TAX
  LDA PPUStatus
  LDA #$00
  STA $2006
  STA $2006
  LDA GraphicsPointersLow,X
  STA BulkTransfer
  LDA GraphicsPointersHigh,X
  STA BulkTransfer+1
  LDA GraphicsBanks,X
  JSR BankswitchProgram
  LDY #$00
  LDX #$20
.Loop
  LDA [BulkTransfer],Y
  STA $2007
  INY
  BNE .Loop
  INC BulkTransfer+1
  DEX
  BNE .Loop
  RTS

PalettesToPPU:
  TAX
  LDA PPUStatus
  LDA #$3F
  STA $2006
  LDA #$00
  STA $2006
  LDA PalletePointersLow,X
  STA BulkTransfer
  LDA PalettePointersHigh,X
  STA BulkTransfer+1
  LDA #$0F ;Palette Bank
  JSR BankswitchProgram
  LDY #$1F
.Loop:
  LDA [BulkTransfer],Y
  STA $2007
  DEY
  BPL .Loop
  RTS

ScreenToPPU:
  TAX
  LDA PPUStatus
  LDA #$20
  STA $2006
  LDA #$00
  STA $2006
  LDA NametablePointersLow,X
  STA BulkTransfer
  LDA NametablePointersHigh,X
  STA BulkTransfer+1
  LDA NametableBanks,X
  JSR BankswitchProgram
  LDX #$04
  LDY #$00
.Loop
  LDA [BulkTransfer],Y
  STA $2007
  INY
  BNE .Loop
  INC BulkTransfer+1
  DEX
  BNE .Loop
  RTS

BankswitchControl:
  LDY #$80
  STY ZeroWrite
  JMP MainBankSwitch
BankswitchCharacterLower:
  LDY #$A0
  STY ZeroWrite
  JMP MainBankSwitch
BankswitchCharacterUpper:
  LDY #$C0
  STY ZeroWrite
  JMP MainBankSwitch
BankswitchProgram:
  LDY #$E0
  STY ZeroWrite
MainBankSwitch:
  AND #$1F
  STA $8000
  LSR A
  STA $8000
  LSR A
  STA $8000
  LSR A
  STA $8000
  LSR A
  STA [Zero],Y
  RTS

SixteenBitAdd:
  CLC
  LDA SixteenBitMathPrimary+1
  ADC SixteenBitMathSecondary+1
  STA SixteenBitMathResult+1
  LDA SixteenBitMathPrimary
  ADC SixteenBitMathSecondary
  STA SixteenBitMathResult
  RTS
  
SixteenBitSubtract:
  SEC
  LDA SixteenBitMathPrimary+1
  SBC SixteenBitMathSecondary+1
  STA SixteenBitMathResult+1
  LDA SixteenBitMathPrimary
  SBC SixteenBitMathSecondary
  STA SixteenBitMathResult
  RTS
  
WordToScreenFromDictionary:
  TAX
  LDA StringPointersLow,X
  STA BulkTransfer
  LDA StringPointersHigh,X
  STA BulkTransfer+1
  LDA StringBanks,X
  JSR BankswitchProgram
  LDY #$00
.Loop
  LDA [BulkTransfer],Y
  BEQ .RTS
  STA $2007
  INY
  JMP .Loop
.RTS:
  RTS
  
WaitForNMI:
  PHA
.Loop
  LDA Frame
  CMP Frame
  BEQ .Loop
  PLA
  RTS