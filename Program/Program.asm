;Made by 3GenGames, Oct. 18th 2011. Personal use only, but feel free to distribute this package. Use this program at your own risk. And most importantly, make your emulators more accurate.

  .inesprg 16
  .ineschr 0
  .inesmap 1
  .inesmir 1

PPUCtrl=$2000;
PPUMask=$2001;
PPUStatus=$2002;

  .rsset $0000
Zero: .rs 1
ZeroWrite: .rs 1
BulkTransfer: .rs 2
  .rsset $0300
Frame: .rs 1
StartupBank: .rs 1
CurrentBankCheck: .rs 1
Data: .rs 16
SRAMEnabled: .rs 16
SRAMDisabled: .rs 16
SRAMDisabledWrites: .rs 16
SRAMInstalled: .rs 1
SRAMBoot: .rs 1
SRAMDisableable: .rs 1
SRAMDisabledWritesFlag: .rs 1
SixteenBitMathPrimary: .rs 2
SixteenBitMathSecondary: .rs 2
SixteenBitMathResult: .rs 2


  .include "Program/Banks/Bank00.asm"
  .include "Program/Banks/Bank01.asm"
  .include "Program/Banks/Bank02.asm"
  .include "Program/Banks/Bank03.asm"
  .include "Program/Banks/Bank04.asm"
  .include "Program/Banks/Bank05.asm"
  .include "Program/Banks/Bank06.asm"
  .include "Program/Banks/Bank07.asm"
  .include "Program/Banks/Bank08.asm"
  .include "Program/Banks/Bank09.asm"
  .include "Program/Banks/Bank11.asm"
  .include "Program/Banks/Bank12.asm"
  .include "Program/Banks/Bank13.asm"
  .include "Program/Banks/Bank14.asm"
  .include "Program/Banks/Bank15.asm"
  .include "Program/Banks/Bank16.asm"
  .include "Program/Banks/Bank17.asm"
  .include "Program/Banks/Bank18.asm"
  .include "Program/Banks/Bank19.asm"
  .include "Program/Banks/Bank20.asm"
  .include "Program/Banks/Bank21.asm"
  .include "Program/Banks/Bank22.asm"
  .include "Program/Banks/Bank23.asm"
  .include "Program/Banks/Bank24.asm"
  .include "Program/Banks/Bank25.asm"
  .include "Program/Banks/Bank26.asm"
  .include "Program/Banks/Bank27.asm"
  .include "Program/Banks/Bank28.asm"
  .include "Program/Banks/Bank29.asm"
  .include "Program/Banks/Bank30.asm"
  .include "Program/Banks/Bank31.asm"