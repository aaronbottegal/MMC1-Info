  .bank 31
  .org $E000
MainReset:
  TAY
  .include "Program/Startup.asm"
  STY StartupBank
  .include "Program/SRAMCompareLoops.asm"
BankCheck:
  LDA #$0F
  JSR BankswitchControl
  LDA #$00
  JSR BankswitchCharacterLower
  LDA #$01
  JSR BankswitchCharacterUpper
  LDA #$00
  JSR GraphicsToPPU
  LDA #$00
  JSR PalettesToPPU
  LDA #$00
  JSR ScreenToPPU
  LDX #$0F
BankTestLoop:
  STX CurrentBankCheck
  TXA
  JSR BankswitchProgram
  LDA $BFF3
  AND #$7F
  CMP CurrentBankCheck
  BNE .WrongBank
  LDA #$01
.WriteBankData:
  STA Data,X
  DEX
  BPL BankTestLoop
  BMI ResultsToScreen
.WrongBank:
  LDA #$03
  BNE .WriteBankData
ResultsToScreen:
  LDX #$07
  LDA #$00
  STA SixteenBitMathSecondary
  LDA #$20
  STA SixteenBitMathSecondary+1
  LDA #$21
  STA SixteenBitMathPrimary
  LDA #$BA
  STA SixteenBitMathPrimary+1
.ResultsToScreenLoop:
  JSR SixteenBitSubtract
  LDA PPUStatus
  LDA SixteenBitMathResult
  STA $2006
  LDA SixteenBitMathResult+1
  STA $2006
  LDA Data+8,X
  STA $2007
  STA $2007
  STA $2007
  STA $2007
  LDA SixteenBitMathResult
  STA $2006
  LDA SixteenBitMathResult+1
  SEC
  SBC #$11
  STA $2006
  LDA Data,X
  STA $2007
  STA $2007
  STA $2007
  STA $2007
  LDA SixteenBitMathResult+1
  STA SixteenBitMathPrimary+1
  LDA SixteenBitMathResult
  STA SixteenBitMathPrimary
  DEX
  BPL .ResultsToScreenLoop

  LDA #$21
  STA $2006
  LDA #$F1
  STA $2006
  LDA SRAMInstalled
  JSR WordToScreenFromDictionary
  LDA SRAMInstalled
  CMP #$03
  BEQ .OtherInfo
  
  LDA #$22
  STA $2006
  LDA #$0E
  STA $2006
  LDA SRAMBoot
  JSR WordToScreenFromDictionary
  
  LDA #$22
  STA $2006
  LDA #$33
  STA $2006
  LDA SRAMDisableable
  JSR WordToScreenFromDictionary

  LDA #$22
  STA $2006
  LDA #$57
  STA $2006
  LDA SRAMDisabledWritesFlag
  JSR WordToScreenFromDictionary

.OtherInfo:
  LDA #$10
  JSR BankswitchProgram ;Disable MMC1 SRAM for next boot up.
  LDA #$21
  STA $2006
  LDA #$B1
  STA $2006
  LDA StartupBank
  AND #$7F
  LSR A
  LSR A
  LSR A
  LSR A
  TAX
  LDA HexToScreenTable,X
  STA $2007
  LDA StartupBank
  AND #$0F
  TAX
  LDA HexToScreenTable,X
  STA $2007
ShowScreen:
  LDA #%10001000
  STA PPUCtrl
  JSR WaitForNMI
  LDA PPUStatus
  LDA #$00
  STA $2005
  STA $2005
  LDA #%00011110
  STA PPUMask
.NMILoop:
  JSR WaitForNMI
  LDA PPUStatus
  LDA #$00
  STA $2005
  STA $2005
  LDA #$00
  STA $2003
  LDA #$02
  STA $4014
  JMP .NMILoop
  
MainNMI:
  INC Frame
MainIRQ:
  RTI

  .include "Program/Subroutines.asm"
  
HexToScreenTable:
  .db $30,$31,$32,$33,$34,$35,$36,$37,$38,$39,$41,$42,$43,$44,$45,$46

NametablePointersLow:
  .db LOW(MainNametable)
  
NametablePointersHigh:
  .db HIGH(MainNametable)
  
NametableBanks:
  .db $0F
  
GraphicsPointersLow:
  .db LOW(MainGraphics)
  
GraphicsPointersHigh:
  .db HIGH(MainGraphics)
  
GraphicsBanks:
  .db $0F

PalletePointersLow:
  .db LOW(TitlePalette)

PalettePointersHigh:
  .db HIGH(TitlePalette)

StringPointersLow:
  .db LOW(DictionaryWord0)
  .db LOW(DictionaryWord1)
  .db LOW(DictionaryWord2)
  .db LOW(DictionaryWord3)
  .db LOW(DictionaryWord4)
  .db LOW(DictionaryWord5)
  .db LOW(DictionaryWord6)
  
StringPointersHigh:
  .db HIGH(DictionaryWord0)
  .db HIGH(DictionaryWord1)
  .db HIGH(DictionaryWord2)
  .db HIGH(DictionaryWord3)
  .db HIGH(DictionaryWord4)
  .db HIGH(DictionaryWord5)
  .db HIGH(DictionaryWord6)
  
StringBanks:
  .db $0F,$0F,$0F,$0F,$0F

TitlePalette:
  .db $0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F
  .db $0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$17,$30,$29,$0F
HeaderGood: .db $FF,$00,$FF,$00,$FF,$00,$47,$6F,$6F,$64,$48,$65,$61,$64,$65,$72
HeaderBad: .db $42,$61,$64,$48,$65,$61,$64,$65,$72,$FF,$00,$FF,$00,$FF,$00,$FF
DictionaryWord0: .db "Disabled",$00
DictionaryWord1: .db "Enabled",$00
DictionaryWord2: .db "Yes",$00
DictionaryWord3: .db "No",$00
DictionaryWord4: .db "Bank",$00
DictionaryWord5: .db "Accepted",$00
DictionaryWord6: .db "Rejected",$00
MainNametable: .incbin "Screens/MainNametable.nam"
  
  .org $FFF0
  .db $00
MMC1GlitchBoot15:
  SEI
  LDA #$8F
  STA $8040
  JMP MainReset
  .dw MainNMI ;NMI
  .dw MMC1GlitchBoot15 ;Reset
  .dw MainIRQ ;IRQ
  