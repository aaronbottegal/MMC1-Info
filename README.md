NES MMC1 Test.


This NES program will boot on an NES MMC1 cart and give MMC1 boot state information, along with tests bank switching with indirect indexed addressing.

Compile by running NESASM3 on Program/Program.asm.